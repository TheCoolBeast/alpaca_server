<?php

/*
    Authors:
    Daniel Olea Martín <dom.olea@gmail.com>
    Bruno Ordozgoiti Rubio <ordozgoitipiedras@gmail.com>

    Copyright (c) 2013 Zaka the Alpaca
    All Rights Reserved.

    BANANAPPS CONFIDENTIAL
    __________________
    NOTICE: All information contained herein is, and remains
    the property of Bananapps and its suppliers, if any. The
    intellectual and technical concepts contained herein are
    proprietary to Bananapps and its suppliers and may be
    covered by U.S. and Foreign Patents, patents in process,
     and are protected by trade secret or copyright law.
    Dissemination of this information or reproduction of this
    material is strictly forbidden unless prior written
    permission is obtained from Bananapps.
*/

class Definition {
    //Python-ish, allows json_encode to work properly
    public $definition;
    public $type;
    
    public function __construct($definition, $type) {
        $this->definition = $definition;
        $this->type = $type;
    }
    
    public function toJson() {
        return json_encode(array(
            'definition' => $this->definition,
            'type' => $this->type
        ));
    }

}
