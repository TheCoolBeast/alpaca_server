<?php

//Authors:
//Daniel Olea Martín <dom.olea@gmail.com>
//Bruno Ordozgoiti Rubio <ordozgoitipiedras@gmail.com>
//
//Copyright (c) 2013 Zaka the Alpaca
//All Rights Reserved.
//
//BANANAPPS CONFIDENTIAL
//__________________
//NOTICE: All information contained herein is, and remains
//the property of Bananapps and its suppliers, if any. The
//intellectual and technical concepts contained herein are
//proprietary to Bananapps and its suppliers and may be
//covered by U.S. and Foreign Patents, patents in process,
// and are protected by trade secret or copyright law.
//Dissemination of this information or reproduction of this
//material is strictly forbidden unless prior written
//permission is obtained from Bananapps.

    header('Content-type: application/json');
    
    include_once './Definition.php';
    include_once './Word.php';

    if(isset($_GET['query'])){
        //000webhost adds a script at the end of the json (bravo!)
        define('Webhost_NIAPA', '/*');
        define('Webhost_NIAPA_Empty', '{}/*');
        
        define(
            'DEFINITION_QRY',
            'SELECT word, definition, type FROM Entry WHERE word = :word'
        );
        
        try{
            $config = parse_ini_file('config/alpaca.ini', true);
            
            //Refactoring ahead
            $dbh = new PDO(
                'mysql:host=' . $config['db_config']['host'] . 
                ';dbname=' . $config['db_config']['db'], 
                $config['db_config']['user'], 
                $config['db_config']['password']
            );
            
            
            
            $stmt = $dbh->prepare(DEFINITION_QRY, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
            $stmt->execute(array('word' => $_GET['query']));
            
            //Check for errors
            if($stmt->errorCode() != 0) {
                $error = $stmt->errorInfo();
                throw new PDOException($error[2]);
            }
            
            $definitions = array();
            $result = $stmt->fetchAll();
            
            //Iterate through the array to obtain all definitions
            foreach ($result as $i => $field){
                $definitions[] = new Definition($field['definition'], $field['type']);
            }
            
            if(!empty($definitions)){
                //Instead of trusting the user input, use the word we have stored in the DB.
                $word = new Word($result[0]['word'], $definitions);
                print $word->toJson() . Webhost_NIAPA;
            } else {
                print Webhost_NIAPA_Empty;
            }
        
        //If not catched might expose db credentials (turtle...)
        }catch(PDOException $ex){
            //TODO: error code?
            echo 'DB error';
        }
    }
?>